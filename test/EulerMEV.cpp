//! Used to test the function ff::atomic::mev
#include "ff/Edge.h"
#include "ff/EulerOperations.h"
#include "ff/Face.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"

namespace {
ff::Polyhedron cube({{{{0, 1, 2, 3}, false},
                      {{7, 6, 5, 4}, false},
                      {{4, 5, 1, 0}, false},
                      {{5, 6, 2, 1}, false},
                      {{6, 7, 3, 2}, false},
                      {{7, 4, 0, 3}, false}},
                     false},
                    {
                        {-1, -1, -1},
                        {-1, 1, -1},
                        {1, 1, -1},
                        {1, -1, -1},
                        {-1, -1, 1},
                        {-1, 1, 1},
                        {1, 1, 1},
                        {1, -1, 1},
                    });
}

//! Tests whether the result of a kill-edge-face operation changes topology correctly.

TEST_CASE("MEV:LeftFace", "")
{
    ff::Polyhedron* p = ff::atomic::mev(cube, 0, 1, {-1, 0, -1});
    CHECK(p->faces()[0].edges().size() == 5);
    CHECK(p->vertices()[p->vertices().size() - 1] == R3(-1, 0, -1));
    CHECK(p->topology().faces[0].vertexIndices == std::vector<int>{0, 8, 1, 2, 3});
}

//! Tests whether mev will throw when the vertex already exists

TEST_CASE("MEV:THROWS:VertexExists", "")
{
    CHECK_THROWS(ff::atomic::mev(cube, 0, 1, {-1, 1, -1}));
    CHECK_THROWS(ff::atomic::mev(cube, 0, 1, {-1, -1, -1}));
}

//! Tests whether mev will throw when the edge does not exist

TEST_CASE("MEV:THROWS:EdgeDoesNotExist", "")
{
    CHECK_THROWS(ff::atomic::mev(cube, 0, 10, {-1, 1, -1}));
}
