#include "ff/Prism.h"
#include "ff/Face.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"

#define CHECK_NEAR(a, b, eps) CHECK(fabs((a)-(b)) < eps);

TEST_CASE("asPolyhedron.prism", "")
{
    ff::Prism prism(false, 1, {{-0.5, -0.5, 0}, {-0.5, 0.5, 0}, {0.5, 0.5, 0}, {0.5, -0.5, 0}});
    ff::Polyhedron* polyhedron = prism.asPolyhedron();

    CHECK(polyhedron->faces().size() == 6);

    CHECK_NEAR(polyhedron->vertices()[4].z(), 0.5, 1E-13);
    CHECK_NEAR(polyhedron->vertices()[5].z(), 0.5, 1E-13);
    CHECK_NEAR(polyhedron->vertices()[6].z(), 0.5, 1E-13);
    CHECK_NEAR(polyhedron->vertices()[7].z(), 0.5, 1E-13);

    CHECK(polyhedron->faces()[0].normal() == R3(0, 0, -1));
    CHECK(polyhedron->faces()[1].normal() == R3(-1, 0, 0));
    CHECK(polyhedron->faces()[2].normal() == R3(0, 1, 0));
    CHECK(polyhedron->faces()[3].normal() == R3(1, 0, 0));
    CHECK(polyhedron->faces()[4].normal() == R3(0, -1, 0));
    CHECK(polyhedron->faces()[5].normal() == R3(0, 0, 1));
}

TEST_CASE("asPolyhedron.symmetricPrism", "")
{
    ff::Prism prism(true, 1, {{-0.5, -0.5, 0}, {-0.5, 0.5, 0}, {0.5, 0.5, 0}, {0.5, -0.5, 0}});
    ff::Polyhedron* polyhedron = prism.asPolyhedron();
    CHECK(polyhedron->faces().size() == 3);
}
