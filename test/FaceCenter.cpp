//! Tests of function Face::center_of_polygon.

#include "ff/Face.h"
#include "test/3rdparty/catch.hpp"

#define CHECK_NEAR(a, b, eps) CHECK(fabs((a)-(b)) < eps);

//! Square centered in the origin.

TEST_CASE("FaceCenter:CenteredRectangle", "")
{
    ff::Face face({{-0.5, -1.4, 0}, {-0.5, 1.4, 0}, {0.5, 1.4, 0}, {0.5, -1.4, 0}}, true);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0, 1E-13);
    CHECK_NEAR(center.y(), 0, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}

//! Ditto without symmetry flag.

TEST_CASE("FaceCenter:CenteredRectangle:nosym", "")
{
    ff::Face face({{-0.5, -1.4, 0}, {-0.5, 1.4, 0}, {0.5, 1.4, 0}, {0.5, -1.4, 0}}, false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0, 1E-13);
    CHECK_NEAR(center.y(), 0, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}

//! Off-center square.

TEST_CASE("FaceCenter:OffCenter", "")
{
    ff::Face face({{0, 0, 0}, {0, 1, 0}, {1, 1, 0}, {1, 0, 0}}, false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0.5, 1E-13);
    CHECK_NEAR(center.y(), 0.5, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}

//! Off-center square, shifted in z.

TEST_CASE("FaceCenter:OffZ", "")
{
    ff::Face face({{0, 0, 3}, {0, 1, 3}, {1, 1, 3}, {1, 0, 3}}, false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0.5, 1E-13);
    CHECK_NEAR(center.y(), 0.5, 1E-13);
    CHECK_NEAR(center.z(), 3, 1E-13);
}

//! Rectangle rotated around x axis, centered in the origin.

TEST_CASE("FaceCenter:Rectangle:rotX", "")
{
    ff::Face face({{-0.5, -0.5, 0.5}, {-0.5, 0.5, 0.5}, {0.5, 0.5, -0.5}, {0.5, -0.5, -0.5}}, true);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0, 1E-13);
    CHECK_NEAR(center.y(), 0, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}

TEST_CASE("FaceCenter:Rectangle:rotX:nosym", "")
{
    ff::Face face({{-0.5, -0.5, 0.5}, {-0.5, 0.5, 0.5}, {0.5, 0.5, -0.5}, {0.5, -0.5, -0.5}},
                  false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0, 1E-13);
    CHECK_NEAR(center.y(), 0, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}

//! Trapezoid.

TEST_CASE("FaceCenter:Trapezoid", "")
{
    ff::Face face({{-1.0, -0.5, 7}, {-0.5, 0.5, 7}, {0.5, 0.5, 7}, {1.0, -0.5, 7}}, false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0, 1E-13);
    CHECK_NEAR(center.y(), -1. / 18., 1E-13);
    CHECK_NEAR(center.z(), 7, 1E-13);
}

//! Asymmetric pentagon.

TEST_CASE("FaceCenter:Pentagon", "")
{
    ff::Face face({{2, 4, 0}, {4, 2, 0}, {1, -3, 0}, {-3, -2, 0}, {-2, 2, 0}}, false);
    const R3 center = face.center_of_polygon();
    CHECK_NEAR(center.x(), 0.395480225988701, 1E-13);
    CHECK_NEAR(center.y(), 0.423728813559322, 1E-13);
    CHECK_NEAR(center.z(), 0, 1E-13);
}
