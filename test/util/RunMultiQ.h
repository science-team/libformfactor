#ifndef FORMFACTOR_TEST_UTIL_RUNMULTIQ_H
#define FORMFACTOR_TEST_UTIL_RUNMULTIQ_H

#include <functional>
#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>

//! Driver for FormFactorSpecializationTest and FormFactorSymmetryTest.

namespace formfactorTest {

//! Runs tests of fff0(q) vs fff1(q) for a huge number of different vectors q.
//! The callback functions fff0/1 may compute ff0(q) vs ff1(q), or ff(q) vs ff(q').
int run_test_for_many_q(std::function<complex_t(C3)> fff0, std::function<complex_t(C3)> fff1,
                        double qmag_min, double qmag_max, double eps, bool real_only = false);

} // namespace formfactorTest

#endif // FORMFACTOR_TEST_UTIL_RUNMULTIQ_H
