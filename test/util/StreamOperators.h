#ifndef FORMFACTOR_TEST_UTIL_STREAMOPERATORS_H
#define FORMFACTOR_TEST_UTIL_STREAMOPERATORS_H

#include <heinz/Vectors3D.h>
#include <ostream>
#include <vector>

// Stream operators injected into namespace std to be recognized by catch2

namespace std {
std::ostream& operator<<(std::ostream& os, const std::vector<R3>& vertices)
{
    os << "{";
    for (const R3& vertex : vertices)
        os << "{" << vertex.x() << ", " << vertex.y() << ", " << vertex.z() << "}, ";
    os << "}";
    return os;
}
}; // namespace std

#endif // FORMFACTOR_TEST_UTIL_STREAMOPERATORS_H
