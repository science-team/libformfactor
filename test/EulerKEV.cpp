//! Used to test the function ff::atomic::kev
#include "ff/EulerOperations.h"
#include "ff/Face.h"
#include "ff/Make.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"

namespace {
ff::Box* cube = ff::make::Box(2, 2, 2);
}

//! Tests whether kev throws with invalid input

TEST_CASE("KEV:ThrowsInvalidIds", "")
{
    CHECK_THROWS(ff::atomic::kev(ff::Polyhedron({{}, true}, {}), -1, 1));
    CHECK_THROWS(ff::atomic::kev(ff::Polyhedron({{}, true}, {}), 1, -1));
    CHECK_THROWS(ff::atomic::kev(ff::Polyhedron({{}, true}, {}), 1, 100));
    CHECK_THROWS(ff::atomic::kev(ff::Polyhedron({{}, true}, {}), 100, 1));
}

//! Test whether vertex will get removed

TEST_CASE("KEV:UnusedVertexRemoved", "")
{
    ff::Polyhedron* p = ff::atomic::kev(*cube->asPolyhedron(), 0, 1);
    CHECK(p->vertices().size() == 7);
    CHECK(p->faces().size() == 6);
}

//! Ensure that multiple kev will work without creating exceptions

TEST_CASE("KEV:MultipleOperations", "")
{
    ff::Polyhedron* p;
    p = ff::atomic::kev(*cube->asPolyhedron(), 0, 1);
    p = ff::atomic::kev(*p, 0, 2);
    p = ff::atomic::kev(*p, 0, 2);
    CHECK(p->vertices().size() == 5);
    CHECK(p->faces().size() == 4);
}