//! Used to test the method ff::atomic::mef(...)
#include "ff/EulerOperations.h"
#include "ff/Face.h"
#include "ff/Make.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"

namespace {
ff::Polyhedron cube({{
                         {{0, 1, 2, 3}, false},
                         {{4, 9, 7, 6, 8, 5}, false},
                         {{0, 4, 5, 1}, false},
                         {{1, 5, 6, 2}, false},
                         {{2, 6, 7, 3}, false},
                         {{0, 3, 7, 4}, false},
                     },
                     false},
                    {{-1, -1, -1},
                     {-1, 1, -1},
                     {1, 1, -1},
                     {1, -1, -1},
                     {-1, -1, 1},
                     {-1, 1, 1},
                     {1, 1, 1},
                     {1, -1, 1},
                     {0, 1, 1},
                     {0, -1, 1}});

}

//! Tests whether MEF works correctly with indexes in bounds

TEST_CASE("MEF:InsideBounds", "")
{
    ff::Polyhedron* p = ff::atomic::mef(cube, 1, 1, 4);
    CHECK(p->faces().size() == 7);
    CHECK(p->topology().faces[1].vertexIndices == std::vector<int>{9, 7, 6, 8});
    CHECK(p->topology().faces[p->topology().faces.size() - 1].vertexIndices
          == std::vector<int>{4, 9, 8, 5});
}

//! Tests whether MEF handles index array wrapping correctly

TEST_CASE("MEF:BeginWithWrap", "")
{
    ff::Polyhedron* p = ff::atomic::mef(cube, 1, 4, 1);
    CHECK(p->faces().size() == 7);
    CHECK(p->topology().faces[1].vertexIndices == std::vector<int>{9, 7, 6, 8});
    CHECK(p->topology().faces[p->topology().faces.size() - 1].vertexIndices
          == std::vector<int>{4, 9, 8, 5});
}

//! Test whether MEF will throw if the vertex indices are neighbors

TEST_CASE("MEF:THROWSWhenNeighbor", "")
{
    CHECK_THROWS(ff::atomic::mef(cube, 0, 0, 1));
}

//! Test whether MEF will throw if the vertex indices are neighbors wrapped around the array end

TEST_CASE("MEF:THROWSWhenNeighbor:Wrapped", "")
{
    CHECK_THROWS(ff::atomic::mef(cube, 0, 3, 0));
}