#include "ff/IBody.h"
#include "ff/Make.h"
#include "test/3rdparty/catch.hpp"
#include "test/util/RunMultiQ.h"

/*
// *********** cylinders ***************

TEST_CASE("FormfactorOther.HorizontalCylinderAsCylinder", "")
{
    const double R = .3, L = 3;
    const auto* p0 = ff::make::Cylinder(R, L);
    const auto* p1 = ff::make::HorizontalCylinder(R, L);

    int failures = formfactorTest::run_test_for_many_q(
        [&](C3 q) -> complex_t { return p0.formfactor(q); },
        [&](C3 q) -> complex_t { return p1.formfactor(q.rotatedY(M_PI / 2)); }, 1e-99, 100, 2e-15,
        true);
    CHECK(failures == 0);
}

TEST_CASE("FormfactorOther.HorizontalCylinderSlices", "")
{
    const double R = .3, L = 3;
    const auto* p0 = ff::make::HorizontalCylinder(R, L);
    const auto* p1a = ff::make::HorizontalCylinder(R, L, -R, -0.3 * R);
    const auto* p1b = ff::make::HorizontalCylinder(R, L, -0.3 * R, +R);

    int failures = formfactorTest::run_test_for_many_q(
        [&](C3 q) -> complex_t { return p0.formfactor(q); },
        [&](C3 q) -> complex_t {
            return p1a.formfactor(q) + exp_I(q.z() * 0.7 * R) * p1b.formfactor(q);
        },
        1e-99, 100, 1.2e-3, true);
    CHECK(failures == 0);
}
*/
