//! Used to test function ff::atomic::kef
#include "ff/EulerOperations.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"
namespace {
ff::Polyhedron cube({{
                         {{0, 1, 2, 3}, false},
                         {{4, 9, 8, 5}, false},
                         {{6, 8, 9, 7}, false},
                         {{0, 4, 5, 1}, false},
                         {{1, 5, 6, 2}, false},
                         {{2, 6, 7, 3}, false},
                         {{0, 3, 7, 4}, false},
                     },
                     false},
                    {{-1, -1, -1},
                     {-1, 1, -1},
                     {1, 1, -1},
                     {1, -1, -1},
                     {-1, -1, 1},
                     {-1, 1, 1},
                     {1, 1, 1},
                     {1, -1, 1},
                     {0, 1, 1},
                     {0, -1, 1}});
}

//! Test whether vertexIndices are correct after the operation

TEST_CASE("KEF:VertexIDOrderLR", "")
{

    ff::Polyhedron* p = ff::atomic::kef(cube, 8, 9);
    CHECK(p->topology().faces[p->topology().faces.size() - 1].vertexIndices
          == std::vector<int>{9, 7, 6, 8, 5, 4});
    delete p;
}

//! Dito but with switching faces

TEST_CASE("KEF:VertexIDOrderRL", "")
{

    ff::Polyhedron* p = ff::atomic::kef(cube, 9, 8);
    CHECK(p->topology().faces[p->topology().faces.size() - 1].vertexIndices
          == std::vector<int>{8, 5, 4, 9, 7, 6});
    delete p;
}

//! Test weather function throws with invalid input

TEST_CASE("KEF:ThrowsInvalidEdge", "")
{
    // valid but not common edge
    CHECK_THROWS(ff::atomic::kef(cube, 1, 3));
    // invalid vertex ids
    CHECK_THROWS(ff::atomic::kef(cube, 20, 21));
    // invalid edge with valid vertex ids
    CHECK_THROWS(ff::atomic::kef(cube, 1, 8));
}