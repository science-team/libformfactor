#include "ff/Face.h"
#include "ff/Make.h"
#include "ff/Polyhedron.h"
#include "test/3rdparty/catch.hpp"

#define CHECK_NEAR(a, b, eps) CHECK(fabs((a)-(b)) < eps);

void test_platonic(const ff::Polyhedron& P)
{
    double area = P.faces()[0].area();
    for (const ff::Face& F : P.faces())
        CHECK_NEAR(F.area(), area, area * 1e-14);
    double volume = P.faces()[0].pyramidalVolume();
    for (const ff::Face& F : P.faces())
        CHECK_NEAR(F.pyramidalVolume(), volume, volume * 1e-14);
}


TEST_CASE("Platonic", "")
{
    test_platonic(*ff::make::Tetrahedron(1.));
    test_platonic(*ff::make::Octahedron(1.));
    test_platonic(*ff::make::Dodecahedron(1.));
    test_platonic(*ff::make::Icosahedron(1.));
}
