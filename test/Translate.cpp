#include "ff/Make.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include "test/3rdparty/catch.hpp"

TEST_CASE("Translated.Ci", "")
{
    const ff::Polyhedron* f0 = ff::make::Octahedron(1.);
    std::vector<R3> vv1;
    for (const R3& v : f0->vertices()) // arbitrary offset
        vv1.push_back(v + R3{0.2, -0.7, 11.1});

    // If the center of mass is not at the origin, then we must set the Ci bit to false:
    CHECK_THROWS(new ff::Polyhedron(f0->topology(), vv1));
    const ff::Topology t1{f0->topology().faces, false};
    CHECK_NOTHROW(new ff::Polyhedron(t1, vv1));
}
