#include "ff/Factorial.h"
#include "test/3rdparty/catch.hpp"
#include <memory>

namespace {
constexpr auto ReciprocalFactorialArray = ff_aux::generateReciprocalFactorialArray<171>();

} // namespace


TEST_CASE("FactorialTest", "")
{
    CHECK(ReciprocalFactorialArray.size() > 150);
    CHECK(ReciprocalFactorialArray[0] == Approx(1.));
    CHECK(ReciprocalFactorialArray[1] == Approx(1.));
    CHECK(ReciprocalFactorialArray[2] == Approx(0.5));
    CHECK(ReciprocalFactorialArray[3] == Approx(1.0 / 6));
    CHECK(ReciprocalFactorialArray[150] == Approx(1.75027620692601519e-263).epsilon(1e-15));
}
