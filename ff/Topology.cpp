//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Topology.cpp
//! @brief     implements classes PolygonalTopology, Topology
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#include "ff/Topology.h"
#include <cstddef>

bool ff::FacialTopology::operator==(const ff::FacialTopology& rhs) const
{
    return rhs.vertexIndices == vertexIndices && rhs.symmetry_S2 == symmetry_S2;
}

//! Returns a vector of edges (i,j) with vertex indices ordered i < j.
std::vector<std::pair<int, int>> ff::FacialTopology::edges_undirected() const
{
    std::vector<std::pair<int, int>> result;
    for (size_t i = 0; i < vertexIndices.size(); i++) {
        if (vertexIndices[i] < vertexIndices[(i + 1) % vertexIndices.size()])
            result.emplace_back(vertexIndices[i], vertexIndices[(i + 1) % vertexIndices.size()]);
        else
            result.emplace_back(vertexIndices[(i + 1) % vertexIndices.size()], vertexIndices[i]);
    }
    return result;
}

//! Returns a vector of edges (i,j) with i,j in the order defined in vertexIndices.
std::vector<std::pair<int, int>> ff::FacialTopology::edges_directed() const
{
    std::vector<std::pair<int, int>> result;
    for (size_t i = 0; i < vertexIndices.size(); i++) {
        result.emplace_back(vertexIndices[i], vertexIndices[(i + 1) % vertexIndices.size()]);
    }
    return result;
}

bool ff::Topology::operator==(const Topology& rhs) const
{
    return rhs.faces == faces && rhs.symmetry_Ci == symmetry_Ci;
}