//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Edge.h
//! @brief     Defines class Edge
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_EDGE_H
#define FF_EDGE_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <vector>

namespace ff {
//! One edge of a polygon, for form factor computation.
class Edge {
public:
    Edge(R3 Vlow, R3 Vhig);

    R3 E() const { return m_E; }
    R3 R() const { return m_R; }
    complex_t qE(C3 q) const { return m_E.dot(q); }
    complex_t qR(C3 q) const { return m_R.dot(q); }

    complex_t contrib(int m, C3 qpa, complex_t qrperp) const;

private:
    R3 m_E; //!< vector pointing from mid of edge to upper vertex
    R3 m_R; //!< position vector of edge midpoint
};
} // namespace ff

#endif // FF_EDGE_H
