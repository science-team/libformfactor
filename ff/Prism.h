//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Prism.h
//! @brief     Defines class Prism.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_PRISM_H
#define FF_PRISM_H

#include "ff/IBody.h"
#include <memory>

namespace ff {

class Face;
class Polyhedron;

class Prism : public IBody {
public:
    Prism(bool symmetry_Ci, double height, const std::vector<R3>& base_vertices,
          const R3& location = {});
    ~Prism() override;

    double radius() const override { return m_radius; }
    double volume() const override { return m_volume; }
    const std::vector<R3>& vertices() const override;
    const Topology& topology() const;
    double z_bottom() const override;
    R3 center_of_mass() const override { return location(); }

    complex_t formfactor_at_center(const C3& q) const override;

    Polyhedron* asPolyhedron() const;

protected:
    // given geometry:
    const double m_height;
    const std::vector<R3> m_base_vertices;

    // precomputed cache:
    const std::unique_ptr<const Face> m_base;
    const double m_radius;
    const double m_volume;

    // on-demand computed once:
    mutable std::vector<R3> my_vertices;
    mutable std::unique_ptr<Topology> my_topology;
};

} // namespace ff

#endif // FF_PRISM_H
