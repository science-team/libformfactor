//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Box.cpp
//! @brief     Implements class Box.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

//! The mathematics implemented here is described in full detail in a paper
//! by Joachim Wuttke, entitled
//! "Form factor (Fourier shape transform) of polygon and polyhedron."

#include "ff/Box.h"
#include "ff/Edge.h"
#include "ff/Face.h"
#include "ff/Polyhedron.h"
#include "ff/Topology.h"
#include <algorithm>
#include <numeric>
#include <stdexcept>

namespace {

complex_t sinc(const complex_t z) // cardinal sine function, sin(x)/x
{
    if (z == complex_t(0., 0.))
        return 1.0;
    return std::sin(z) / z;
}

std::vector<R3> base_vertices(double length, double width)
{
    const double a = length / 2;
    const double b = width / 2;
    return {{+a, +b, 0}, {+a, -b, 0}, {-a, -b, 0}, {-a, +b, 0}};
}

} // namespace


ff::Box::Box(double length, double width, double height, const R3& location)
    : Prism(true, height, base_vertices(length, width), location), m_length(length), m_width(width)
{
}

ff::Box::~Box() = default;

complex_t ff::Box::formfactor_at_center(const C3& q) const
{
    complex_t qzHdiv2 = m_height / 2 * q.z();
    return m_length * m_width * m_height * sinc(m_length / 2 * q.x()) * sinc(m_width / 2 * q.y())
           * sinc(qzHdiv2);
}
