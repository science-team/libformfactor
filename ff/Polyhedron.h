//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Polyhedron.h
//! @brief     Defines class Polyhedron.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_POLYHEDRON_H
#define FF_POLYHEDRON_H

#include "ff/IBody.h"
#include <memory>

namespace ff {

class Face;

//! A polyhedron, implementation class for use in IFormFactorPolyhedron

class Polyhedron : public IBody {
public:
    Polyhedron(const Topology& topology, const std::vector<R3>& vertices, const R3& location = {});
    ~Polyhedron() override;

    double radius() const override { return m_radius; }
    double volume() const override { return m_volume; }
    complex_t formfactor_at_center(const C3& q) const override;
    const std::vector<R3>& vertices() const override { return m_absolute_vertices; }
    const std::vector<R3>& relative_vertices() const { return m_vertices; }
    double z_bottom() const override;
    R3 center_of_mass() const override;
    const std::vector<Face>& faces() const;
    //! Returns wether a vertex v is located inside of the polyhedron.
    bool is_inside(const R3& v, int nsamples = 2) const;

    const Topology& topology() const;
    //! Returns a copy of this Polyhedron clipped with the residing body constrained by zMin,zMax.
    Polyhedron* clipped(double zMin,double zMax) const;
private:
    // given geometry
    const std::unique_ptr<const Topology> m_topology;
    const std::vector<R3> m_vertices; // relative to location

    // caches, precomputed from given geometry
    const std::vector<R3> m_absolute_vertices; // relative to origin
    const std::vector<Face> m_faces;
    const double m_radius;
    const double m_volume;
};

} // namespace ff

#endif // FF_POLYHEDRON_H
