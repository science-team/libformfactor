//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/IBody.cpp
//! @brief     Implements abstract base class IBody.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#include "ff/IBody.h"

complex_t ff::IBody::formfactor(const C3& q) const
{
    return exp_I(m_location.dot(q)) * formfactor_at_center(q);
}
