//  ************************************************************************************************
//
//  BornAgain: simulate and fit reflection and scattering
//
//! @file      ff/Make.h
//! @brief     Defines factory functions in namespace make
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see COPYING)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @authors   Scientific Computing Group at MLZ (see CITATION, AUTHORS)
//
//  ************************************************************************************************

#ifndef FF_MAKE_H
#define FF_MAKE_H

#include "ff/Box.h"
#include "ff/Polyhedron.h"
#include "ff/Prism.h"

namespace ff::make {

ff::Box* Box(double length, double width, double height);
ff::Prism* Prism3(double base_edge, double height);
ff::Prism* Prism6(double base_edge, double height);

ff::Polyhedron* Tetrahedron(double edge);
ff::Polyhedron* Octahedron(double edge);
ff::Polyhedron* Dodecahedron(double edge);
ff::Polyhedron* Icosahedron(double edge);

ff::Polyhedron* Pyramid2(double length, double width, double height, double alpha);
ff::Polyhedron* Pyramid3(double base_edge, double height, double alpha);
ff::Polyhedron* Pyramid4(double base_edge, double height, double alpha);
ff::Polyhedron* Pyramid6(double base_edge, double height, double alpha);
ff::Polyhedron* Bipyramid4(double length, double height, double height_ratio, double alpha);

ff::Polyhedron* CantellatedCube(double length, double removed_length);
ff::Polyhedron* TruncatedCube(double length, double removed_length);

} // namespace ff::make

#endif // FF_MAKE_H
