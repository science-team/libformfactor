//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/IBody.h
//! @brief     Defines abstract base class IBody.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FORMFACTOR_FF_IBODY_H
#define FORMFACTOR_FF_IBODY_H

#include <heinz/Complex.h>
#include <heinz/Vectors3D.h>
#include <vector>

namespace ff {

class Topology;

//! This interface class is the base for all body shape classes

class IBody {
public:
    IBody(const R3& location) : m_location(location) {}
    virtual ~IBody() = default;

    //! Since data members are const, there is no good reason to copy.
    IBody(const IBody&) = delete;

    void move(const R3& location) { m_location += location; }

    virtual double volume() const = 0;
    virtual double radius() const = 0;                   //!< radius of enclosing sphere
    R3 location() const { return m_location; }           //!< position of center of mass
    virtual const std::vector<R3>& vertices() const = 0; //!< in absolute coordinates
    virtual double z_bottom() const = 0; //!< vertical distance between bottom and center
    virtual R3 center_of_mass() const = 0;

    complex_t formfactor(const C3& q) const;
    virtual complex_t formfactor_at_center(const C3& q) const = 0;

    //! Returns whether a point p is located inside of the body.
    // virtual bool is_inside(const R3& p, int nsamples = 2) const;

private:
    R3 m_location;
};

} // namespace ff

#endif // FORMFACTOR_FF_IBODY_H
