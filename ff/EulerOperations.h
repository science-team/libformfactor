//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/EulerOperations.h
//! @brief     Defines atomic operations
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_EULEROPERATIONS_H
#define FF_EULEROPERATIONS_H

#include "ff/Polyhedron.h"
#include <heinz/Vectors3D.h>

namespace ff::atomic {
//! Returns a new Polyhedron with the faces containing the edge (iV0, iV1) removed.
Polyhedron* kef(const Polyhedron& polyhedron, int iV0, int iV1);
//! Kills one edge one vertex, where iV0 and iV1 are the indices of .vertices()
Polyhedron* kev(const Polyhedron& polyhedron, int iV0, int iV1);
//! Makes one vertex one edge, where iV0 and iV1 are the indices of .vertices()
Polyhedron* mev(const Polyhedron& polyhedron, int iV0, int iV1, const R3& v);
//! Makes one edge one face, where iF ist the face index and iE0,iE1 are the vertex indices
Polyhedron* mef(const Polyhedron& polyhedron, int iF, int iE0, int iE1);
//! Clips a Polyhedron by the z_cut offset xy-plane
Polyhedron* z_clip(const Polyhedron& polyhedron, double z_cut,bool keepBelow = false);
} // namespace ff::atomic

#endif // FF_EULEROPERATIONS_H
