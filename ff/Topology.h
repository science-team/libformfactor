//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Topology.h
//! @brief     Defines classes PolygonalTopology, Topology
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_TOPOLOGY_H
#define FF_TOPOLOGY_H

#include <ostream>
#include <vector>

namespace ff {

//! For internal use in PolyhedralFace.
class FacialTopology {
public:
    std::vector<int> vertexIndices;
    bool symmetry_S2;
    bool operator==(const FacialTopology& rhs) const;
    std::vector<std::pair<int, int>> edges_undirected() const;
    std::vector<std::pair<int, int>> edges_directed() const;
};

//! For internal use in IFormFactorPolyhedron.
class Topology {
public:
    std::vector<FacialTopology> faces;
    bool symmetry_Ci;
    bool operator==(const Topology& rhs) const;
};

} // namespace ff

#endif // FF_TOPOLOGY_H
