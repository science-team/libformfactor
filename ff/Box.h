//  ************************************************************************************************
//
//  libformfactor: efficient and accurate computation of scattering form factors
//
//! @file      ff/Box.h
//! @brief     Defines class Box.
//!
//! @homepage  https://jugit.fz-juelich.de/mlz/libformfactor
//! @license   GNU General Public License v3 or higher (see LICENSE)
//! @copyright Forschungszentrum Jülich GmbH 2022
//! @author    Joachim Wuttke, Scientific Computing Group at MLZ (see CITATION)
//
//  ************************************************************************************************

#ifndef FF_BOX_H
#define FF_BOX_H

#include "ff/Prism.h"

namespace ff {

class Face;
class Polyhedron;

class Box : public Prism {
public:
    Box(double length, double width, double height, const R3& location = {});
    ~Box() override;

    complex_t formfactor_at_center(const C3& q) const override;

private:
    // given geometry:
    const double m_length;
    const double m_width;
};

} // namespace ff

#endif // FF_BOX_H
