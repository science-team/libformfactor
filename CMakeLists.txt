cmake_minimum_required(VERSION 3.15 FATAL_ERROR)

project(formfactor VERSION 0.3.1 LANGUAGES CXX)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
include(PreventInSourceBuilds)

set(CMAKE_PROJECT_DESCRIPTION # for display in installer
    "libformfactor - computes Fourier shape transforms.")

## Options.

if(NOT DEFINED BUILD_SHARED_LIBS)
    option(BUILD_SHARED_LIBS "Build as shared library" ON)
endif()

## Compiler settings.

set(CMAKE_CXX_STANDARD 20)

if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP")
    set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS}")
    set(MS_NOWARN "/wd4018 /wd4068 /wd4101 /wd4244 /wd4267 /wd4305 /wd4715 /wd4996")
    set(MS_D "-D_CRT_SECURE_NO_WARNINGS -D_SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING")
    set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} ${MS_NOWARN} ${MS_D}")
    set(CTEST_CONFIGURATION_TYPE "${JOB_BUILD_CONFIGURATION}")
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_BINARY_DIR}/bin/$<CONFIG>)

else()
    option(WERROR "Treat warnings as errors" OFF)
    option(PEDANTIC "Compile with pedantic warnings" ON)
    if(PEDANTIC)
        add_compile_options(-pedantic -Wall)
    endif()
    if(WERROR)
        add_compile_options(-Werror)
    endif()

    add_compile_options(-fno-omit-frame-pointer)
    if (CMAKE_BUILD_TYPE MATCHES Debug)
        add_compile_options(-g)
    else()
        add_compile_options(-O3)
    endif()

    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/lib")
endif()

## Dependences.

find_package(LibHeinz REQUIRED)
message(STATUS "LibHeinz: found=${LibHeinz_FOUND}, include_dirs=${LibHeinz_INCLUDE_DIR}, "
    "version=${LibHeinz_VERSION}")

## Source directory.

add_subdirectory(ff)

## Tests.

include(CTest)
add_subdirectory(test)

## CPack settings.

include(InstallRequiredSystemLibraries)
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_VERSION_MAJOR "${formfactor_VERSION_MAJOR}")
set(CPACK_PACKAGE_VERSION_MINOR "${formfactor_VERSION_MINOR}")
set(CPACK_PACKAGE_VERSION_PATCH "${formfactor_VERSION_PATCH}")
set(CPACK_SOURCE_GENERATOR "TGZ")
include(CPack)

## Config files.

include(CMakePackageConfigHelpers)
configure_package_config_file("${CMAKE_CURRENT_SOURCE_DIR}/Config.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/formfactorConfig.cmake"
    INSTALL_DESTINATION cmake
    NO_SET_AND_CHECK_MACRO
    NO_CHECK_REQUIRED_COMPONENTS_MACRO    )

write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/formfactorConfigVersion.cmake"
    VERSION "${formfactor_VERSION}"
    COMPATIBILITY AnyNewerVersion    )

install(FILES
    "${PROJECT_BINARY_DIR}/formfactorConfig.cmake"
    "${PROJECT_BINARY_DIR}/formfactorConfigVersion.cmake"
    DESTINATION cmake)

## Export targets.

install(EXPORT formfactorTargets
  FILE formfactorTargets.cmake
  DESTINATION cmake)

export(EXPORT formfactorTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/formfactorTargets.cmake")
